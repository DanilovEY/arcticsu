package ru.arcticsu.activities.gui;

import ru.arcticsu.R.drawable;
import ru.arcticsu.tools.sites.arcticsuru.elements.ParseElementFeed;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * @author Danilov E.Y.
 *
 */
public class FeedItemContent extends GuiElement
{
	private LinearLayout content;
	private Context context;
	private ParseElementFeed parseElement;
	 /**
     * {@linkplain FeedItemContent}
     */
    public FeedItemContent(Context context, ParseElementFeed parseElement)
    {
    	this.context = context;
	    this.parseElement = parseElement;
	    this.init();
    }
    
    private void init()
    {
    	GradientDrawable drawable = new GradientDrawable();
    	drawable.setShape(GradientDrawable.RECTANGLE);
    	drawable.setStroke(3, Color.parseColor("#1b496c"));
    	drawable.setCornerRadius(8);
    	drawable.setColor(Color.parseColor("#1b496c"));    	
    	
    	LinearLayout border = new LinearLayout(this.context);
    	
    	border.setBackgroundDrawable(drawable);

    	border.setPadding(5, 5, 5, 5);
    	
        TextView feedTitleText = new TextView(this.context);
        feedTitleText.setLayoutParams(new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        ));
        feedTitleText.setTextColor(Color.parseColor("#f9f9f9"));
        feedTitleText.setText(parseElement.getTitle());
        feedTitleText.setTextSize(16);
        feedTitleText.setTypeface(null, Typeface.BOLD);
        feedTitleText.setGravity(Gravity.CENTER);

        TextView feedShortContentText = new TextView(this.context);
        LinearLayout.LayoutParams paramFeedShortContentText = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );

        //paramFeedShortContentText.weight = (float) 1.0;
        feedShortContentText.setLayoutParams(paramFeedShortContentText);
        feedShortContentText.setText(this.parseElement.getShortContent());
        feedShortContentText.setTextSize(13);
        //feedShortContentText.setMaxLines(2);
        
        border.setGravity(Gravity.CENTER);
        border.addView(feedTitleText);
        
        LinearLayout.LayoutParams paramFeedContent = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        paramFeedContent.setMargins(20, 0, 20, 20);
        
        this.content = new LinearLayout(this.context);
        this.content.setLayoutParams(paramFeedContent);
        this.content.setOrientation(LinearLayout.VERTICAL);
        this.content.addView(border);
        this.content.addView(feedShortContentText);
        

    }
    
	@Override
    public void update()
    {
		
    }

	@Override
    public View getView()
    {
	    return this.content;
    }
	
}
