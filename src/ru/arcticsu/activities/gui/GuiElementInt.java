package ru.arcticsu.activities.gui;

import android.view.View;

/**
 * @author Danilov E.Y.
 *
 */
public interface GuiElementInt
{
	public View getView();
	public void update();
}
