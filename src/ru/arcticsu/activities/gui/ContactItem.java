/**
 * 
 */
package ru.arcticsu.activities.gui;

import ru.arcticsu.tools.sites.arcticsuru.elements.ParseElementContacts;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.UnderlineSpan;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * @author Danilov
 *
 */
public class ContactItem extends GuiElement
{
	private LinearLayout content;
	private Context context;
	private ParseElementContacts parseElement;
	
	public ContactItem(Context context, ParseElementContacts parseElement)
	{
		this.parseElement = parseElement;
		this.context = context;
		this.init();
	}
	
	private void init()
	{
		LinearLayout.LayoutParams contentParam = new LinearLayout.LayoutParams(
		        ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
		contentParam.setMargins(0, 10, 0, 0);
		
		this.content = new LinearLayout(this.context);
		this.content.setOrientation(LinearLayout.VERTICAL);
		this.content.setLayoutParams(contentParam);
		this.content.setGravity(Gravity.CENTER);
		
		TextView name = new TextView(this.context);
		name.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, 
				ViewGroup.LayoutParams.WRAP_CONTENT));
		name.setText(this.parseElement.getName());
		name.setGravity(Gravity.CENTER);
		name.setTextSize(24);
		name.setTypeface(null, Typeface.BOLD);
		
		TextView degree = new TextView(this.context);
		degree.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, 
				ViewGroup.LayoutParams.WRAP_CONTENT));
		degree.setText(this.parseElement.getDegree());
		degree.setGravity(Gravity.CENTER);
		degree.setTextSize(10);
		
		Spannable text = new SpannableString(this.parseElement.getNumberPhone());
		text.setSpan(new UnderlineSpan(), 0, this.parseElement.getNumberPhone().length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		
		TextView number = new TextView(this.context);
		number.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, 
				ViewGroup.LayoutParams.WRAP_CONTENT));
		number.setText(text);
		number.setGravity(Gravity.CENTER);
		number.setTextSize(17);
		
		this.content.addView(name);
		this.content.addView(number);
		this.content.addView(degree);
		
	}
	
    @Override
    public View getView()
    {
	    return this.content;
    }


    @Override
    public void update()
    {
	    
    }

}
