/**
 * 
 */
package ru.arcticsu.activities;

import java.util.ArrayList;

import ru.arcticsu.R;
import ru.arcticsu.activities.gui.ContactItem;
import ru.arcticsu.activities.gui.FeedItemContent;
import ru.arcticsu.tools.sites.arcticsuru.ConstantsArctisuru;
import ru.arcticsu.tools.sites.arcticsuru.ParseArcticsuContacts;
import ru.arcticsu.tools.sites.arcticsuru.elements.ParseElementContacts;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;
import android.widget.Toast;

/**
 * @author Danilov
 */
public class ContactsActivity extends RunnableActivity
{
	private final String TABS_TAG_1 = "Tag 1";
	private final String TABS_TAG_2 = "Tag 2";
	private final String TAB_IPM = "ИПМ";
	private final String TAB_ECO = "Экономический";
	private final String TAB_GYM = "Гуманитарный";
	private final String TAB_PHY = "Физико-энергетический";
	private final String TAB_ECOLOG = "Экологический";
	private final String TAB_MIN = "Горный";
	private Handler hen;
	private TabHost.TabContentFactory tabFactory;
	private LinearLayout contactsIPM;
	private LinearLayout contactsECO;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		getLayoutInflater().inflate(R.layout.runnable_activity, frameLayout);

		/**
		 * Setting title and itemChecked
		 */
		mDrawerList.setItemChecked(position, true);
		setTitle(listArray[position]);

		// setContentView(R.layout.activity_main);
		LinearLayout linearLayout1 = (LinearLayout) findViewById(R.id.linearLayoutContainer);
		linearLayout1.setGravity(Gravity.FILL);

		this.tabFactory = new TabHost.TabContentFactory()
		{

			@Override
			public View createTabContent(String tag)
			{
				if(tag == TABS_TAG_1)
				{
					return contactsIPM;
				}
				else
					if(tag == TABS_TAG_2)
					{ 
						return contactsECO; 
					}

				return null;
			}
		};

		// Create the TabWidget (the tabs)
		TabWidget tabWidget = new TabWidget(this);
		tabWidget.setGravity(Gravity.FILL);
		tabWidget.setId(android.R.id.tabs);

		// Create the FrameLayout (the content area)
		FrameLayout frame = new FrameLayout(this);
		frame.setId(android.R.id.tabcontent);
		LinearLayout.LayoutParams frameLayoutParams = new LinearLayout.LayoutParams(
		        LinearLayout.LayoutParams.FILL_PARENT,
		        LinearLayout.LayoutParams.WRAP_CONTENT, 1);
		frameLayoutParams.setMargins(0, 0, 0, 0);
		frame.setLayoutParams(frameLayoutParams);

		// Create the container for the above widgets
		LinearLayout tabHostLayout = new LinearLayout(this);
		tabHostLayout.setLayoutParams(frameLayoutParams);
		tabHostLayout.setOrientation(LinearLayout.VERTICAL);
		tabHostLayout.addView(tabWidget);
		tabHostLayout.addView(frame);

		// Create the TabHost and add the container to it.
		TabHost tabHost = new TabHost(this, null);
		tabHost.setLayoutParams(frameLayoutParams);
		tabHost.addView(tabHostLayout);
		tabHost.setup();

		linearLayout1.addView(tabHost);

		this.hen = new Handler()
		{
			public void handleMessage(Message msg)
			{
				super.handleMessage(msg);
				if(msg.what == 0)
				{
					ContactItem feed = (ContactItem) msg.obj;

					contactsIPM.addView(feed.getView());
				}
				else
					if(msg.what == 1)
					{
						ContactItem feed = (ContactItem) msg.obj;

						contactsECO.addView(feed.getView());
					}
			}
		};

		this.contactsIPM = new LinearLayout(ContactsActivity.this);
		this.contactsIPM.setOrientation(LinearLayout.VERTICAL);
		
		this.contactsECO = new LinearLayout(ContactsActivity.this);
		this.contactsECO.setOrientation(LinearLayout.VERTICAL);

		new Thread(new Runnable()
		{
			public void run()
			{
				ArrayList<ContactItem> contItem = new ArrayList<ContactItem>();

				ParseArcticsuContacts arcticsu = new ParseArcticsuContacts(ConstantsArctisuru.URL_ARCTICSU_RU_CONTACTS_IPM);
				
				if(arcticsu.getContactList() != null)
				{
					for(ParseElementContacts cont : arcticsu.getContactList())
					{
						contItem.add(new ContactItem(ContactsActivity.this,cont));
					}
				}

				if(contItem.size() == 0) 
					Toast.makeText(ContactsActivity.this,"Не удалось подключится к www.arcticsu.ru",Toast.LENGTH_LONG).show();
				else 
					for(ContactItem contIt : contItem)
					{
						Message mes = new Message();
						mes.obj = contIt;
						mes.what = 0;
						hen.sendMessage(mes);
					}
			}
		}).start();

		new Thread(new Runnable()
		{
			public void run()
			{
				ArrayList<ContactItem> contItem = new ArrayList<ContactItem>();

				ParseArcticsuContacts arcticsu = new ParseArcticsuContacts(ConstantsArctisuru.URL_ARCTICSU_RU_CONTACTS_ECO);
				
				if(arcticsu.getContactList() != null)
				{
					for(ParseElementContacts cont : arcticsu.getContactList())
					{
						contItem.add(new ContactItem(ContactsActivity.this,cont));
					}
				}

				if(contItem.size() == 0) 
					Toast.makeText(ContactsActivity.this,"Не удалось подключится к www.arcticsu.ru",Toast.LENGTH_LONG).show();
				else 
					for(ContactItem contIt : contItem)
					{
						Message mes = new Message();
						mes.obj = contIt;
						mes.what = 1;
						hen.sendMessage(mes);
					}
			}
		}).start();

		TabHost.TabSpec tabSpec;

		tabSpec = tabHost.newTabSpec(TABS_TAG_1);
		tabSpec.setContent(tabFactory);
		tabSpec.setIndicator(this.TAB_IPM);
		tabHost.addTab(tabSpec);

		tabSpec = tabHost.newTabSpec(TABS_TAG_2);
		tabSpec.setContent(tabFactory);
		tabSpec.setIndicator(this.TAB_ECO);
		tabHost.addTab(tabSpec);
		
		tabSpec = tabHost.newTabSpec(TABS_TAG_2);
		tabSpec.setContent(tabFactory);
		tabSpec.setIndicator(this.TAB_GYM);
		tabHost.addTab(tabSpec);

	}
}
