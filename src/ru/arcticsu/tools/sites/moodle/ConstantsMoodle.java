package ru.arcticsu.tools.sites.moodle;

/**
 * @author Danilov E.Y.
 *
 */
public interface ConstantsMoodle
{
	public static final String URL_ARCTICSU_RU = "http://www.arcticsu.ru/content/novosti?page=1";
	public static final String FEED_CLASS_ITEM_LIST = "item-list";
	public static final String FEED_TAG_LI = "li";
	public static final String FEED_TAG_LI_CLASS_VIEWS_ROW = FEED_TAG_LI + ".views-row";
	public static final String FEED_H2 = "h2";
}
