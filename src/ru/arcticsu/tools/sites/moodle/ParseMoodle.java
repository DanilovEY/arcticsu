package ru.arcticsu.tools.sites.moodle;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import ru.arcticsu.tools.sites.Parse;
import ru.arcticsu.tools.sites.arcticsuru.elements.ParseElementFeed;
import android.util.Log;

/**
 * Парсинг сайта Arcticsu.ru.
 * @author Danilov E.Y.
 *
 */
public class ParseMoodle extends Parse
{
	
	private ParseElementFeed[] arrayFeed;
	
	/**
	 * {@linkplain ParseMoodle}
	 */
    public ParseMoodle()
    {
	    super(ConstantsMoodle.URL_ARCTICSU_RU);
	    this.parsing();
    }
    
    public void parsing()
    {
    	Element elemContentContent = this.parseDoc.getElementsByClass(ConstantsMoodle.FEED_CLASS_ITEM_LIST).first();
    	
		Elements elemLi = elemContentContent.select(ConstantsMoodle.FEED_TAG_LI_CLASS_VIEWS_ROW);
		Log.i("Array Size", String.valueOf(elemLi.size()));
		this.arrayFeed = new ParseElementFeed[elemLi.size()];
    	int index = 0;
    	
		for(Element elemFeed : elemLi)
		{			
			this.arrayFeed[index] = new ParseElementFeed(elemFeed);

    		index++;
		}
    }

	public ParseElementFeed[] getArrayFeed()
	{
		return this.arrayFeed;
	}

}
