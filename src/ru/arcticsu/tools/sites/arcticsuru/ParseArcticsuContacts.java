/**
 * 
 */
package ru.arcticsu.tools.sites.arcticsuru;

import java.util.ArrayList;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import android.util.Log;
import ru.arcticsu.tools.sites.Parse;
import ru.arcticsu.tools.sites.arcticsuru.elements.ParseElementContacts;

/**
 * @author Danilov
 *
 */
public class ParseArcticsuContacts extends Parse
{
	private String aboutContacts;
	private ArrayList<ParseElementContacts> contactList;
	
    public ParseArcticsuContacts(String URL)
    {
	    super(URL);
	    
	    this.aboutContacts = "aboutContacts";
	    
	    this.contactList = new ArrayList<ParseElementContacts>();
	    
	    if(this.parseDoc!= null)
	    	this.parsing();
    }

    public void parsing()
    {
    	
    	Element tbody = this.parseDoc.getElementsByTag("tbody").first();
    	
    	Elements tr = tbody.getElementsByTag("tr");
    	
    	this.aboutContacts = tr.get(0).text();
    	
    	for(int i = 1; i<tr.size(); i++)
    	{
    		this.contactList.add(new ParseElementContacts(tr.get(i)));
    	}
    	
    }

	/**
	 * @return the contactList
	 */
	public ArrayList<ParseElementContacts> getContactList()
	{
		return contactList;
	}

	/**
	 * @param contactList the contactList to set
	 */
	public void setContactList(ArrayList<ParseElementContacts> contactList)
	{
		this.contactList = contactList;
	}

	/**
	 * @return the aboutContacts
	 */
	public String getAboutContacts()
	{
		return aboutContacts;
	}

	/**
	 * @param aboutContacts the aboutContacts to set
	 */
	public void setAboutContacts(String aboutContacts)
	{
		this.aboutContacts = aboutContacts;
	}

}
