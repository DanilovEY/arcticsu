package ru.arcticsu.tools.sites.arcticsuru.elements;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * Парсинг контактов.
 * @author Danilov E.Y.
 *
 */
public class ParseElementContacts extends ParseElement
{
	private String name;
	private String numberPhone;
	private String degree;

    public ParseElementContacts(Element element)
    {
	    super(element);
	    this.parsing();
    }

	
	public void parsing()
	{
		Elements td = this.element.getElementsByTag("td");
		
		this.name = td.get(0).text();
		this.degree = td.get(1).text();
		this.numberPhone = td.get(2).text();
		
	}


	/**
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}


	/**
	 * @param name the name to set
	 */
	public void setName(String name)
	{
		this.name = name;
	}


	/**
	 * @return the numberPhone
	 */
	public String getNumberPhone()
	{
		return numberPhone;
	}


	/**
	 * @param numberPhone the numberPhone to set
	 */
	public void setNumberPhone(String numberPhone)
	{
		this.numberPhone = numberPhone;
	}


	/**
	 * @return the degree
	 */
	public String getDegree()
	{
		return degree;
	}


	/**
	 * @param degree the degree to set
	 */
	public void setDegree(String degree)
	{
		this.degree = degree;
	}

}
